'use strict'

const mongoose = require('mongoose')
const FormatDate = new Date('dd-mm-yyyy')


const conf = require('./db-conf')
const Schema = mongoose.Schema
const GastoSchema = new Schema({
    gasto_id: {
        type: String,
        default: ''
    },
    nombre: {
        type: String,
        default: ''
    },
    fecha: {
        type: Date,
        default: Date.now
    },

    costo: Number,
    descripcion: String,
}, {
    collection: "gasto"
})
const GastoModel = mongoose.model("Gasto", GastoSchema)

mongoose.connect(`mongodb:\/\/${conf.mongo.host}/${conf.mongo.db}`)


module.exports = GastoModel
