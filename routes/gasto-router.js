'use strict';

const GastoController = require('../controllers/gasto-controller')
const express = require('express')
const router = express.Router()



router
    .get('/', GastoController.getAll)
    .get('/agregar', GastoController.addForm)
    .post('/', GastoController.save)
    .get('/editar/:gasto_id', GastoController.getOne)
    .put('/actualizar/:gasto_id', GastoController.save)
    .delete('/eliminar/:gasto_id', GastoController.delete)
    .use(GastoController.error404)


module.exports = router
